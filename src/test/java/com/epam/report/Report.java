package com.epam.report;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Report implements ITestListener {
    private Logger log = LogManager.getLogger("CulculatorTest");

    @Override
    public void onTestStart(ITestResult iTestResult) {
        log.trace("onTestStart");
        log.trace(iTestResult);
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        log.trace("onTestSuccess");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        log.trace("onTestFailure");

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        log.trace("onTestSkipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        log.trace("onTestFailedButWithinSuccessPercentage");
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        log.trace("onStart");

    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        log.trace("onFinish");
    }
}
